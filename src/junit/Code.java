package junit;

public class Code {

    public static void main (String[] args){

        Integer [] test = new Integer[]{1,2,3,4,1,2,3,4};

        test = removeDuplicates(test);


        Integer [] test2 = new Integer[]{1,2,3,4,1,2,3,4};

    }



    public static boolean isSpecial(Integer number)
    {
        int remainder = number % 11;

        return remainder == 1 || remainder == 0 ;

    }

    public static Integer longestStreak(String input) {
        if(input.length() == 0) {
            return 0;
        }

        String[] characters = input.split("");

        String lastSymbol = characters[0];
        int streakLenght = 0;
        int longestStreak = 0;

        for (String currentSymbol : characters) {

             if(currentSymbol.equals(lastSymbol)){
                 streakLenght++;
             }
             else{
                streakLenght = 1;
             }

             if(longestStreak < streakLenght){
                 longestStreak = streakLenght;
             }

             lastSymbol = currentSymbol;

            }

        return longestStreak;
    }

    public static Integer[] removeDuplicates(Integer[] input) {
        Integer [] output = new Integer[input.length];
        Integer addToOutput = null;
        boolean isDuplicate = false;
        output[0] = input[0];

        //Iterate through original array.
        for(int i = 0; i <= input.length -1; i++) {
            isDuplicate = false;
            addToOutput = input[i];

            //Iterate through output array to check if already exists.
            for(int j = 0; j <= output.length-1; j++ ){
                if (input[i] == output[j] || input[i] == null){
                    isDuplicate = true;
                    continue;
                }
            }

            if(addToOutput != null && isDuplicate == false){

                //Find first empty slot in array
                for (int a = 0; a <= output.length -1; a++) {
                    if (output[a] == null){
                        output[a] = addToOutput;
                        break;
                    }
                }
            }
        }

        //Reduce array to correct size.
        int x = 0;
        while (output[x] != null){
            x++;
        }

        Integer[] output_trimmed = new Integer[x];

        int y=0;
        while (output[y] != null) {
            output_trimmed[y] = output[y];
            y++;
        }

        return output_trimmed;
    }

    public static Integer sumIgnoringDuplicates(Integer[] integers) {

        Integer sum = 0;

        //Remove duplicates from the input array
        integers = removeDuplicates(integers);

        for (Integer integer : integers) {
            sum += integer;
        }

        return sum;
    }

}
